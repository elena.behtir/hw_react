import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from "prop-types";
import './card.scss';
import { Button } from '../Button/Button';
import { Modal } from '../Modal/Modal';
import {
  isModalAddCardSelector,
  isModalSelectCardSelector,
  cardsSelector,
  favoritListSelector,
  cardListSelector
} from "../../redux/selector";
import {
  ADD_PRODUCT_ACTION_TYPE,
  ADD_SELECT_PRODUCT_ACTION_TYPE,
  IS_MODAL_YES_ACTION_TYPE,
  IS_MODAL_NO_ACTION_TYPE,
  REMOVE_PRODUCT_FROM_FAVORITLIST_ACTION_TYPE,
  ADD_PRODUCT_TO_FAVORITLIST_ACTION_TYPE,
  ADD_PRODUCT_TO_CARDLIST_ACTION_TYPE
} from "../../redux/actions";
// import { getCards } from '../../redux/thunks';


export function Card(props) {

  const cards = useSelector(cardsSelector);
  const favoritList = useSelector(favoritListSelector);
  const cardList = useSelector(cardListSelector);

  // useEffect(() => {
  //   dispatch(getCards())
  // }, [])

  const isModalAddCard = useSelector(isModalAddCardSelector);
  // const isModalSelectCard = useSelector(isModalSelectCardSelector);

  const dispatch = useDispatch();

  // const [isIconStarColor, setIconStarColor] = useState(false ||
  //   localStorage.getItem(props.card.id) === 'true'
  // );
  // const [isIconStarNoColor, setIconStarNoColor] = useState(
  //   isIconStarColor ? false : true
  // );

  useEffect(() => {
    localStorage.setItem(props.card.id, isIconStarColor);
  }, [isIconStarColor, props.card.id]);

  const { name, image, price, artcl } = props.card;

  
  const cardListSelect = (card) => {
    console.log(card)
    localStorage.setItem('favoritList', JSON.stringify([...favoritList]));
    if (favoritList.find(item => item.id === card.id)) {
      dispatch({ type: REMOVE_PRODUCT_FROM_FAVORITLIST_ACTION_TYPE, payload: { card } })
    } else {
      dispatch({ type: ADD_PRODUCT_TO_FAVORITLIST_ACTION_TYPE, payload: { card } })
    }
  }

  const cardListAdd = (card) => {
    localStorage.setItem('cardList', JSON.stringify([...cardList]));
    dispatch({ type: ADD_PRODUCT_TO_CARDLIST_ACTION_TYPE, payload: { card } })
  };

  return (
    <div className="card">
      <div className="card__name">{name}</div>
      <div className="card__wrap">
        <img className="card__img" src={image} alt="#" />
        <div className="card__info">
          <div className="card__info-price">Ціна: {price} грн.</div>
          <div>Артикул: {artcl}</div>
          <div>Колір: {props.color}</div>
        </div>
      </div>
      <div className="card__starsbtn">

        {isIconStarNoColor && <i className="fa-regular fa-star"
          onClick={(e) => {
            dispatch({ type: ADD_SELECT_PRODUCT_ACTION_TYPE })
            cardListSelect(props.card)
          }}

        ></i>}

        {isIconStarColor && <i className="fa-solid fa-star"
          onClick={(e) => {
            setIconStarNoColor(true)
            setIconStarColor(false)
            cardListSelect(props.card)
            console.log("товар видалено в обраного")
          }}
        ></i>}


        <Button className="btn__main" text="Додати в кошик"
          backgroundColor="blueviolet"
          handleClick={(e) => {
            dispatch({ type: ADD_PRODUCT_ACTION_TYPE })
          }} />
      </div>

      {isModalAddCard && (<Modal
        header="Додавання товару у кошик"
        text="Ви дійсно хочете додати цей товар у кошик?"
        backgroundColor="#cc9df7"
        onClose={() => dispatch({ type: IS_MODAL_NO_ACTION_TYPE })}
        actions={
          <>
            <Button className="btn__modal"
              text="Так"
              backgroundColor="rgb(0, 0, 0, 0.3)"
              handleClick={(e) => {
                dispatch({ type: IS_MODAL_YES_ACTION_TYPE })
                cardListAdd(props.card)

                console.log("Товар додано в кошик")
              }} />
            <Button className="btn__modal"
              text="Ні"
              backgroundColor="rgb(0, 0, 0, 0.3)"
              handleClick={(e) => {
                dispatch({ type: IS_MODAL_NO_ACTION_TYPE })
              }} />
          </>}
      />)}


      {/* {isModalSelectCard && (<Modal
        header="Додавання товару в обране"
        text="Ви дійсно хочете додати цей товар в обране?"
        backgroundColor="#4f4ff9"
        onClose={() => {
          dispatch({ type: IS_MODAL_NO_ACTION_TYPE })
        }}
        actions={
          <>
            <Button className="btn__modal"
              text="Так"
              backgroundColor="rgb(0, 0, 0, 0.3)"
              handleClick={(e) => {
                dispatch({ type: IS_MODAL_YES_ACTION_TYPE })
                setIconStarNoColor(false)
                setIconStarColor(true)
                cardListSelect(props.card)
                console.log(props.card)
                console.log("Товар додано в обране")
              }} />

            <Button className="btn__modal"
              text="Ні"
              backgroundColor="rgb(0, 0, 0, 0.3)"
              handleClick={(e) => {
                dispatch({ type: IS_MODAL_NO_ACTION_TYPE })
              }} />
          </>}
      />)} */}

    </div>
  )
}

Card.defaultProps = {
  color: 'в асортименті'
};

Card.propTypes = {
  color: PropTypes.string,
  card: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    artcl: PropTypes.number.isRequired
  }).isRequired
}