import React, { useState } from 'react';
import { useOutletContext } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import './list.scss';
import { Modal } from '../../components/Modal/Modal';
import { Button } from '../../components/Button/Button';
import { isModalDeleteCardSelector } from '../../redux/selector';
import { DELETE_PRODUCT_ACTION_TYPE,
    IS_MODAL_YES_ACTION_TYPE,
    IS_MODAL_NO_ACTION_TYPE
 } from '../../redux/actions';

export function List(props) {

    const { name, image, price, quantity } = props.card;

    const isModalDeleteCard = useSelector(isModalDeleteCardSelector);
    // const [isModalDeleteCard, setModalDeleteCard] = useState(false);

    const dispatch = useDispatch();

    const { cardListDelete } = useOutletContext();

    return (
        <div>
            <div className="list">
                <img className="list__img" src={image} alt="#" />
                <div className="list__name">{name}</div>
                <div>Кількість {quantity} шт.</div>
                <div className="list__info-price">{price} грн.</div>
                <i className="fa-regular fa-circle-xmark list__delete"
                    onClick={(e) => {
                        dispatch({ type: DELETE_PRODUCT_ACTION_TYPE })
                        // (setModalDeleteCard(true))
                    }}                    
                ></i>
            </div>

            {isModalDeleteCard && (<Modal
                header="Видалення товару з кошика"
                text="Ви дійсно хочете видалити цей товар з кошика?"
                backgroundColor="#f64f4f"
                onClose={() => { 
                    dispatch({ type: IS_MODAL_NO_ACTION_TYPE })
                    // setModalDeleteCard(false) 
                }}
                actions={
                    <>
                        <Button className="btn__modal"
                            text="Так"
                            backgroundColor="rgb(0, 0, 0, 0.3)"
                            handleClick={(e) => {
                                dispatch({ type: IS_MODAL_YES_ACTION_TYPE })
                                // setModalDeleteCard(false)
                                cardListDelete(props.card)
                                console.log("Товар видалено з кошика")
                            }} />
                        <Button className="btn__modal"
                            text="Ні"
                            backgroundColor="rgb(0, 0, 0, 0.3)"
                            handleClick={(e) => {
                                dispatch({ type: IS_MODAL_NO_ACTION_TYPE })
                                // setModalDeleteCard(false)
                            }} />
                    </>}
            />)}
        </div>
    )
}
