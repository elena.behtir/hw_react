import React from 'react'
import './button.scss';

export function Button(props) {
     
        return <button className={`btn ${props.className}`}
            style={{backgroundColor: props.backgroundColor}}
            onClick={(e) => {
            props.handleClick()
        }}>{props.text}</button>     
    }
    
