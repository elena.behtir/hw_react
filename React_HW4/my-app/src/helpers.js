export const countTotalPrice = (array) => {
    return array.reduce((accum, next) => {
      return accum += (next.price * next.quantity)
    }, 0)
  }
