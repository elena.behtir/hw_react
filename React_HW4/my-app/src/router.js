import { createBrowserRouter } from "react-router-dom";
import { BasketPage } from "./pages/BasketPage/BasketPage";
import { SelectPage } from "./pages/SelectPage/SelectPage";
import { MainPage } from "./pages/Main/MainPage";
import { FirstPage } from "./pages/Main/FirstPage";


export const router = createBrowserRouter([{
    path: '/',
    element: <FirstPage />,
    children: [
        {
            path: '/',
            element: <MainPage />
        }, {
            path: '/basket',
            element: <BasketPage />
        }, {
            path: '/select',
            element: <SelectPage />
        }
    ]
}])