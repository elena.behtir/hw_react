import React from 'react';
import '../BasketPage/basket.scss';
import { List } from '../../components/List/List';
import { countTotalPrice } from '../../helpers';
import { useSelector } from 'react-redux';
import { cardListSelector } from '../../redux/selector';

export function BasketPage() {

    const cardList = useSelector(cardListSelector);

    const totalPrice = countTotalPrice(cardList);

    return (     

        <div className='main'>
        <div className='basket'>
            <div className="basket__text">Товари у кошику</div>

            {cardList.map((card) => (
                <List
                    key={card.id}
                    card={card}                   
                />
            ))}

            <div className='basket__total-price'>Загальна вартість: {totalPrice} грн.</div>

        </div>

     </div>

    )
}