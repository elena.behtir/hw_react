import React from 'react';
import './select.scss';
import { FavoritList } from '../../components/FavoritList/FavoritList';
import { favoritListSelector } from '../../redux/selector';
import { useSelector } from 'react-redux';


export function SelectPage() {


    const favoritList = useSelector(favoritListSelector);


    return (

        <div className='main'>
            <div className='select'>

                <div className="select__text">Обрані товари</div>

                {favoritList.map((card) => (
                    <FavoritList
                        key={card.id}
                        card={card}
                    />
                ))}

            </div>
        </div>



    )
}
