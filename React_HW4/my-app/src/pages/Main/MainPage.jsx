import React from 'react';
import { useSelector } from 'react-redux';
import { Card } from '../../components/Card/Card';
import { cardsSelector } from '../../redux/selector';

export function MainPage() {

    const cards = useSelector(cardsSelector);

    console.log(cards)

    return (
        <div className="cards">
            {cards.map((card) => {
                return <Card
                    key={card.id}
                    color={card.color}
                    card={card}
                />
                }
            )}
        </div>
    )
}
