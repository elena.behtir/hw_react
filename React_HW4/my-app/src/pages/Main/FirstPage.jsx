import React, { useEffect } from 'react';
import { Outlet } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Header } from '../../components/Header/Header';
import { Footer } from '../../components/Footer/Footer';
import { favoritListSelector, cardListSelector, cardsSelector } from '../../redux/selector';
import {
  REMOVE_PRODUCT_FROM_CARDLIST_ACTION_TYPE
} from '../../redux/actions';
import { getCards } from '../../redux/thunks';
import {
  isModalSelectCardSelector,
} from "../../redux/selector";
import {
  IS_MODAL_YES_ACTION_TYPE,
  IS_MODAL_NO_ACTION_TYPE,
} from "../../redux/actions";
// import { getCards } from '../../redux/thunks';
import { Button } from '../../components/Button/Button';
import { Modal } from '../../components/Modal/Modal'; 

export const FirstPage = () => {

  const cards = useSelector(cardsSelector);
  const cardList = useSelector(cardListSelector);
  const favoritList = useSelector(favoritListSelector);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCards())
  }, [])


  // const cardListAdd = (card) => {
  //   dispatch({ type: ADD_PRODUCT_TO_CARDLIST_ACTION_TYPE, payload: { card } })
  // };

  const cardListSelect = (card) => {
    console.log(card)
    localStorage.setItem('favoritList', JSON.stringify([...favoritList]));
    if (favoritList.find(item => item.id === card.id)) {
      dispatch({ type: REMOVE_PRODUCT_FROM_FAVORITLIST_ACTION_TYPE, payload: { card } })
    } else {
      dispatch({ type: ADD_PRODUCT_TO_FAVORITLIST_ACTION_TYPE, payload: { card } })
    }
  }

  const cardListDelete = (card) => {
    if (cardList.find(item => item.id === card.id)) {
      dispatch({ type: REMOVE_PRODUCT_FROM_CARDLIST_ACTION_TYPE, payload: { card } })
    }
  }

  // useEffect(() => {
  //   localStorage.setItem('cardList', JSON.stringify([...cardList]));
  // }, [cardList])

  const isModalSelectCard = useSelector(isModalSelectCardSelector);

  return (

    <div>

      <Header cardList={cardList} favoritList={favoritList} />

      <Outlet />

      <Footer />

      {isModalSelectCard && (<Modal
        header="Додавання товару в обране"
        text="Ви дійсно хочете додати цей товар в обране?"
        backgroundColor="#4f4ff9"
        onClose={() => {
          dispatch({ type: IS_MODAL_NO_ACTION_TYPE })
        }}
        actions={
          <>
            <Button className="btn__modal"
              text="Так"
              backgroundColor="rgb(0, 0, 0, 0.3)"
              handleClick={(e) => {
                dispatch({ type: IS_MODAL_YES_ACTION_TYPE })
                // setIconStarNoColor(false)
                // setIconStarColor(true)
                cardListSelect(props.card)
                // console.log(props.card)
                console.log("Товар додано в обране")
              }} />

            <Button className="btn__modal"
              text="Ні"
              backgroundColor="rgb(0, 0, 0, 0.3)"
              handleClick={(e) => {
                dispatch({ type: IS_MODAL_NO_ACTION_TYPE })
              }} />
          </>}
      />)}

    </div>
  );
}
