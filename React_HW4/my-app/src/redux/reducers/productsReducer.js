import { GET_CARD_ACTION_TYPE, 
    GET_CARD_SUCCESS_ACTION_TYPE, 
    GET_CARD_ERROR_ACTION_TYPE } from "../actions"

// Редюсер для карток товарів, які відмальовуються на головній сторінці

export function cardsReducer(state = [], action) {
    switch (action.type) {
        case GET_CARD_ACTION_TYPE:
            return [...state]
        case GET_CARD_SUCCESS_ACTION_TYPE:
            return  [ ...state, ...action.payload.cards ]
        case GET_CARD_ERROR_ACTION_TYPE:
            return [ ...state ]
        default:
            return state
    }
}
