import {
    IS_MODAL_YES_ACTION_TYPE,
    IS_MODAL_NO_ACTION_TYPE,
    ADD_PRODUCT_ACTION_TYPE,
    ADD_SELECT_PRODUCT_ACTION_TYPE,
    DELETE_PRODUCT_ACTION_TYPE
} from "../actions";


export function modalReducer(state = { isModalAddCard: false, isModalSelectCard: false, isModalDeleteCard: false, activeProduct: null }, action) {
    switch (action.type) {
        case ADD_PRODUCT_ACTION_TYPE:
            return { isModalAddCard: true, isModalSelectCard: false, isModalDeleteCard: false, action: payload.card }
        case ADD_SELECT_PRODUCT_ACTION_TYPE:
            return { isModalAddCard: false, isModalSelectCard: true, isModalDeleteCard: false }
        case DELETE_PRODUCT_ACTION_TYPE:
            return { isModalAddCard: false, isModalSelectCard: false, isModalDeleteCard: true }
        case IS_MODAL_YES_ACTION_TYPE:
            return { isModalAddCard: false, isModalSelectCard: false, isModalDeleteCard: false }
        case IS_MODAL_NO_ACTION_TYPE:
            return { isModalAddCard: false, isModalSelectCard: false, isModalDeleteCard: false }
        default:
            return state
    }
}