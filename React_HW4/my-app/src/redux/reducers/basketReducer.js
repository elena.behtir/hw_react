import { ADD_PRODUCT_TO_CARDLIST_ACTION_TYPE, 
    REMOVE_PRODUCT_FROM_CARDLIST_ACTION_TYPE } from "../actions";

    
// Редюсер для корзини
const savedCardList = JSON.parse(localStorage.getItem('cardList')) || []

export function cardListReducer(state = savedCardList, action) {
    switch (action.type) {
      case ADD_PRODUCT_TO_CARDLIST_ACTION_TYPE:
        const card = action.payload.card;
              return {
                  ...state,
                  [card]: state[card] ? state[card] + 1 : 1
              };
      case REMOVE_PRODUCT_FROM_CARDLIST_ACTION_TYPE:
        return state.filter(card => card !== action.payload.card)
      default:
        return state
    }
  }