export const favoritListSelector = state => state.favoritList;
export const cardListSelector = state => state.cardList;
export const cardsSelector = state => state.cards;

export const isModalAddCardSelector = state => state.modal.isModalAddCard;
export const isModalSelectCardSelector = state => state.modal.isModalSelectCard;
export const isModalDeleteCardSelector = state => state.modal.isModalDeleteCard;