// actions for SelectPage (favoritList)
export const ADD_PRODUCT_TO_FAVORITLIST_ACTION_TYPE = 'favoritList/addProduct';
export const REMOVE_PRODUCT_FROM_FAVORITLIST_ACTION_TYPE = 'favoritList/removeProduct';
export const TOGGLE_PRODUCT_TO_FAVORITLIST_ACTION_TYPE = 'favoritList/toggleProduct';

// actons for BasketPage (cardList)
export const ADD_PRODUCT_TO_CARDLIST_ACTION_TYPE = 'cardList/addProduct';
export const REMOVE_PRODUCT_FROM_CARDLIST_ACTION_TYPE = 'cardList/removeProduct';

// actions for MainPage (cards)
export const GET_CARD_ACTION_TYPE = 'cards/getCards';
export const GET_CARD_SUCCESS_ACTION_TYPE = 'cards/addCards';
export const GET_CARD_ERROR_ACTION_TYPE = 'cards/errorCards';

// actions for Modal
export const IS_MODAL_YES_ACTION_TYPE = 'modal/getYes';
export const IS_MODAL_NO_ACTION_TYPE = 'modal/getNo';
export const ADD_PRODUCT_ACTION_TYPE = 'modal/addProduct';
export const ADD_SELECT_PRODUCT_ACTION_TYPE = 'modal/selectProduct';
export const DELETE_PRODUCT_ACTION_TYPE = 'modal/deleteProduct';
