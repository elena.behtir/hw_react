import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { cardsReducer } from './reducers/productsReducer';
import { cardListReducer } from './reducers/basketReducer';
import { favoritListReducer } from './reducers/favoritsReducer';
import { modalReducer } from './reducers/modalReducer';


// Збираємо всі редюсери

const rootReducer = combineReducers({
  favoritList: favoritListReducer,
  cardList: cardListReducer,
  cards: cardsReducer,
  modal: modalReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  rootReducer,
  composeEnhancers(
    applyMiddleware(thunk)
  )
);
