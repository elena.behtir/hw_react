import { GET_CARD_SUCCESS_ACTION_TYPE } from "./actions"

export function getCards() {
    return (dispatch) => {
        dispatch({ type: 'GET_CARD_ACTION_TYPE' })
        fetch('./data/products.json', { method: 'GET' })
        .then(result => result.json())
        .then(data => {
            dispatch({ type: GET_CARD_SUCCESS_ACTION_TYPE,
            payload: { cards: data } })
        })
    }
}
