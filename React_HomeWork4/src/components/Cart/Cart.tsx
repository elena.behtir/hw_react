import { FC } from "react";
import { useAppSelector } from "../../hooks/storeHooks";
import { Goods } from "../Goods/Goods";

interface IProps {}

export const Cart: FC<IProps> = ({}) => {
  const cartList = useAppSelector((state) => state.cart.cart);

  const cartRender = cartList.map((goods) => (
    <Goods goods={goods} key={goods.id} btnDelete={true} />
  ));

interface goods {
  price: number;
  cartAmount: number;
}

const calculateCartTotal = (cartList: goods[]): number => {
  return cartList.reduce((total: number, product: goods) => {
    return total + (product.price * product.cartAmount);
  }, 0);
};

const cartTotal = calculateCartTotal(cartList);


  return (
    <>
      <h1 className="page-title">Кошик</h1>
      <ul className="goods-list">{cartRender}</ul>
      <h2 className="total-price">Загальна вартість: <span>{cartTotal}</span> грн.</h2>
    </>
  );
};
