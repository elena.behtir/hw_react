import { useContext } from 'react';
import { SwitchContext } from '../providers/SwitchProvider';


const useSwitch = () => {
    const value = useContext(SwitchContext)

  return value
}

export default useSwitch