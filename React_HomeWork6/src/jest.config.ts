import { Config } from 'jest';


export default async (): Promise<Config> => {
  return {
    verbose: true,
    testEnvironment: 'jsdom',
  };
};


module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  moduleFileExtensions: [
    "js",
    "jsx",
    "ts",
    "tsx"
  ],

  transform: {
    "^.+\\.tsx?$": "ts-jest",
    "^.+\\.scss$": "jest-transform-sass",
    "^.+\.css$": "jest-transform-stub",
    "^.+\\.(ts|tsx)$": "ts-jest",
    "^.+\\.(js|jsx|ts|tsx)$": "babel-jest",
  },

  moduleNameMapper: {
    "\\.(scss)$": "identity-obj-proxy",
    "\.(css|less)$": "identity-obj-proxy"
  },
  // ...
}

