import * as React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';

interface ButtonProps {
  text: string;
  backgroundColor?: string;
  onClick?: () => void;
}

const Button: React.FC<ButtonProps> = ({ text, backgroundColor, onClick }) => {
  return (
    <button style={{ backgroundColor }} onClick={onClick}>
      {text}
    </button>
  );
};

export default Button;

const text: string = 'Click me';
const backgroundColor: string = '#333';
const onClick: jest.Mock = jest.fn();

describe('Button component', () => {

  it('should call the onClick function when clicked', () => {
    render(<Button text={text} onClick={onClick} />);
    const button = screen.getByText(text);
    userEvent.click(button);
    expect(onClick).toHaveBeenCalled();
  });

  it('renders with backgroundColor prop', () => {
    render(<Button text={text} backgroundColor={backgroundColor} />);
    const button = screen.getByText(text);
    expect(button).toHaveStyle({ backgroundColor });
  });
});