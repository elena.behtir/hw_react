import favoritesReducer, { toggleFavorites } from '../store/favorites/favoritesSlice'

describe('favorit reducer', () => {

    const state = { favorites: ['1111', '321423523', '213214312'] }

    test('should remove the same product', () => {
        expect(favoritesReducer(state, toggleFavorites('1111'))).toStrictEqual({ favorites: ['321423523', '213214312'] });
    });

    test('should add new product', () => {
        expect(favoritesReducer(state, toggleFavorites('2222'))).toStrictEqual({ favorites: ['1111', '321423523', '213214312', '2222'] });
    });
});

