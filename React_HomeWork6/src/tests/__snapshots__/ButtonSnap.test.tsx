import * as React from 'react';
import { render } from '@testing-library/react';
import { Button } from '../../components/Button/Button'; 

describe('Button', () => {
  it('should render correctly', () => {
    const { asFragment } = render(<Button btnText="Click me!" />);
    expect(asFragment()).toMatchSnapshot();
  });
});