import { render, screen } from '@testing-library/react';
import * as React from "react";
import { Provider } from 'react-redux';
import configureStore, { MockStore } from 'redux-mock-store';
import { Modal } from '../components/Modal/Modal';

const mockStore = configureStore([]);

describe('Modal', () => {
    let store: MockStore;

    beforeEach(() => {
        store = mockStore({
            modal: {
                addGoodsIsOpen: false,
                removeGoodsIsOpen: false,
                makePurchaseIsOpen: false,
            },
        });
    });

    it('should render', () => {
        render(
            <Provider store={store}>
                <Modal />
            </Provider>

        );
    });
});