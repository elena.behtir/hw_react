const sass = require('node-sass');

module.exports = {
  process(src, filename) {
    if (filename.match(/\.scss$/)) {
      const result = sass.renderSync({ file: filename });
      return {
        code: result.css.toString(),
        map: null,
      };
    }
    return src;
  },
};