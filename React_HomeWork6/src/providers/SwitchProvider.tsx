import * as React from 'react';
import { createContext, Dispatch, SetStateAction, useState } from "react";

type typeSetState<T> = Dispatch<SetStateAction<T>>

interface IContext {
  type: 'Grid' | 'Flex';
  setType: typeSetState<'Grid' | 'Flex'>;
}

interface ISwitchProviderProps {
  children: React.ReactNode;
}

export const SwitchContext = createContext<IContext>({ 
  type: 'Flex', 
  setType: () => 'Crid' 
});

export const SwitchProvider = ({ children }: ISwitchProviderProps) => {
  const [type, setType] = useState<'Grid' | 'Flex'>('Grid');

  const value: IContext = {
    type,
    setType,
  };

  return (
    <SwitchContext.Provider value={value}>
      {children}
    </SwitchContext.Provider>
  )
}




// import * as React from 'react';
// import { createContext, Dispatch, SetStateAction, useState } from "react";

// type typeSetState<T> = Dispatch<SetStateAction<T>>

// interface IContext {
//     type: 'Grid' | 'Flex'
//     setType: typeSetState<string>
// }

// export const SwitchContext = createContext({ isFlex: false })

// export const SwitchProvider = ({ children }) => {
//     const [isFlex, setIsFlex] = useState(false)
    

//     return (
//         <SwitchContext.Provider value={value}>
//             {children}
//         </SwitchContext.Provider>
//     )
// }
