import { FC } from "react";
import * as React from 'react';
import "./Button.css";

interface IProps {
  btnFunction?: () => void;
  btnText?: string;
  btnClasses?: string;
  btnCross?: boolean;
  [restProps: string]: any;
}

export const Button: FC<IProps> = ({
  btnClasses,
  btnText,
  btnFunction,
  btnCross,
  ...restProps
}: IProps) => {
  return (
    <button className={btnClasses} onClick={btnFunction} {...restProps}>
      {btnText && <span>{btnText}</span>}
      {btnCross && <p>X</p>}
    </button>
  );
};