import * as React from 'react';
import classNames from 'classnames';
import useSwitch from '../../hooks/useSwitch';

interface ILayoutProps {
  children: React.ReactNode;
}

export const Layout = ({ children }: ILayoutProps) => {
  const { type } = useSwitch();
  const flexClass = type === "Flex";

  return (
    <div className={classNames('layout', { type: flexClass })}>
      {children}
    </div>
  )
}

export default Layout;
